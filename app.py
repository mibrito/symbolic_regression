import argparse
from gpsr import (
    setSeed,
    train,
)


def main(train_file_name, population, generations, max_tree):
    train(train_file_name, population, generations, max_tree)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Symbolic Regression')
    parser.add_argument('-s', help="Set seed", default=1, dest="seed", type=int)
    parser.add_argument('--max_tree', help="Max tree size", default=2, dest="max_tree", type=int)
    parser.add_argument('--pop', help="Population size", default=50, dest="population", type=int)
    parser.add_argument('--gen', help="Generation size", default=10, dest="generation", type=int)
    parser.add_argument('--train', help="Train file", dest="train_file_name")
    args = parser.parse_args()

    if args.train_file_name:
        setSeed(args.seed)
        main(args.train_file_name, args.population, args.generation, args.max_tree)

    else:
        parser.print_help()
