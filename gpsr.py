import operator
import random
import time

from math import sqrt
from joblib import Parallel, delayed
import pathos.multiprocessing as mp
from pathos.pools import (ProcessPool as PP, ThreadPool as PT, ParallelPool as PL)

FUNCTIONS = {
  '+': (operator.add, 2),
  '-': (operator.sub, 2),
  '*': (operator.mul, 2),
  '/': (operator.div, 2),
  '^': (operator.pow, 2)
}


def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print '%s function took %0.3f ms' % (f.func_name, (time2-time1)*1000.0)
        return ret
    return wrap


# Aux functions ============================
def setSeed(s):
    random.seed(s)


def terminals(line):
    terminal_set = map(str, range(0, len(line)))
    terminal_set.append('const')

    return terminal_set


# IO functions ============================
def read_dataset(name):
    f = open(name, 'rU')
    ls = f.read().split('\n')

    # removes last \n that generates a empty line
    if ls[len(ls)-1] is '':
        ls = ls[:len(ls)-1]

    for i in range(0, len(ls)):
        l = ls[i].split(',')
        ls[i] = (map(float, l[:len(l)-1]), float(l[len(l)-1]))

    return ls


def prog_str(prog):
    if type(prog) is str:
        return '(arg_{})'.format(prog)
    elif type(prog) is int:
        return '({})'.format(prog)
    else:
        op, (l, r) = prog
        return '({} {} {})'.format(prog_str(l), op, prog_str(r))


# GP functions ============================
def individuo_grow(function_set, terminal_set, max_depth):
    verify_grow = len(terminal_set)/(len(terminal_set) + len(function_set))

    if max_depth is 0 or random.random() < verify_grow:
        expr = random.sample(terminal_set, 1)[0]
        if expr is 'const':
            expr = random.randint(1, 9)
    else:
        func = random.sample(function_set, 1)[0]
        op, arity = FUNCTIONS[func]
        args = [individuo_grow(function_set, terminal_set, max_depth - 1) for i in range(0, arity)]
        expr = (func, tuple(args))

    return expr


def apply_gp(prog, x):
    if type(prog) is str:
        return x[int(prog)]
    elif type(prog) is int:
        return prog
    else:
        op, (l, r) = prog
        op, _ = FUNCTIONS[op]
        return op(apply_gp(l, x), apply_gp(r, x))


def theta(ind, x, y):
    return (apply_gp(ind, x) - y)**2


# evaluate fitness O(train_dataset)
# @timing
def rmse(ind, dataset):
    if True:
        sum_squares = 0
        for x, y in dataset:
            sum_squares = sum_squares + theta(ind, x, y)
    else:
        # pool = PT(nodes=4)
        # squares = pool.uimap(lambda (x, y): theta(ind, x, y), dataset)
        squares = Parallel(n_jobs=2)(delayed(theta)(ind, x, y) for x, y in dataset)
        sum_squares = sum(squares)

    return sqrt(sum_squares/len(dataset))


# Top level functons ============================
# train function
def train(train_file_name, population, generations, max_tree):
    # read train dataset
    train_dataset = read_dataset(train_file_name)

    # function set
    function_set = ['+', '-', '*']

    # terminal set
    terminal_set = terminals(train_dataset[0][0])

    # generate a population
    pop = [individuo_grow(function_set, terminal_set, max_tree) for ind in range(0, population)]

    try:
        cpus = mp.cpu_count()
    except NotImplementedError:
        cpus = 2

    # evaluate fitness O(population * train_dataset)
    if False:
        fitness = [rmse(ind, train_dataset) for ind in pop]
    else:
        fitness = Parallel(n_jobs=2)(delayed(rmse)(ind, train_dataset) for ind in pop)
        # pool = PT(nodes=4)
        # fitness = pool.imap(lambda ind: rmse(ind, train_dataset), pop)
        # fitness = list(fitness)

    better = fitness.index(min(fitness))
    print better, fitness[better], prog_str(pop[better])
    # select individuos (tournament)

    # crossover (p = .90)

    # mutation (p'= 1 - .90 = .10)

    # all new ? diversity ?

    # go back fitness until G generations

    # return best individuo
